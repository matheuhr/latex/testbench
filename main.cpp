#include <iostream>
#include <Latex.h>

int main() {
  std::cout << "LaTeX Testbench" << std::endl;

  std::string str = "\\frac{2}{\\sqrt{3}}";
  std::vector<char> latexFormula(str.begin(), str.end());
  auto img = LaTeX::Interpret(latexFormula, 400, 400);

  std::cout << "Image Size (HxW): " << img->getHeight() << "x" << img->getWidth() << std::endl;

  img->Save("latex.bmp");

  return 0;
}